from django.forms import ModelForm
from django import forms
from .models import Recruit, Answer


class RecruitForm(ModelForm):
    class Meta:
        model = Recruit
        fields = ['name', 'planet']


class MakeHandForm(ModelForm):
    class Meta:
        model = Recruit
        fields = ['sith']
        widgets = {'sith': forms.HiddenInput()}


class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = '__all__'
        widgets = {'recruit': forms.HiddenInput(), 'question': forms.HiddenInput()}
