from . import views
from django.urls import path

urlpatterns = [
    path('', views.index, name='index'),
    path('sith/select/', views.sith_select, name='sith_select'),
    path('sith/<int:id>/', views.sith, name='sith'),
    path('recruit/<int:recruit>', views.recruit_answers, name='recruit_answers'),
    path('recruit/', views.recruit_signup, name='recruit_signup'),
    path('recruit/test/<int:recruit>/<int:question>', views.recruit_test, name='recruit_test'),
    path('recruit/test/complete', views.recruit_test_completed, name='recruit_test_completed'),
    path('bonus/', views.bonus, name='bonus')
]
