from django.shortcuts import render, redirect
from django.db.models import Count
from .models import Sith, Recruit, Answer, Question, Test
from .forms import RecruitForm, AnswerForm, MakeHandForm


def index(request):
    return render(request, 'main/index.html')


def sith_select(request):
    sith_list = Sith.objects.all()
    return render(request, 'main/sith_list.html', {'sith_list': sith_list})


def sith(request, id):
    if 'my' in request.GET and request.GET['my'] == 'true':
        recruit_list = Recruit.objects.filter(sith=Sith.objects.get(id=id))
        title = 'Ваши руки тени'
    else:
        recruit_list = Recruit.objects.filter(planet=Sith.objects.get(id=id).planet)
        title = 'Рекруты с вашей планеты'
    return render(request, 'main/sith.html', {'sith_id': id, 'recruit_list': recruit_list, 'title': title})


def recruit_signup(request):
    if request.method == "POST":
        form = RecruitForm(request.POST)
        if form.is_valid():
            new_recruit = form.save()
            return redirect('recruit_test', recruit=new_recruit.id, question=1)
    return render(request, 'main/recruit_signup.html', {'form': RecruitForm()})


def recruit_test(request, recruit, question):
    recruit = Recruit.objects.get(id=recruit)
    test = Question.objects.filter(test=Test.objects.get(Planet=recruit.planet))

    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            form.save()
            if len(test) == question:
                return redirect('recruit_test_completed')
            else:
                return redirect('recruit_test', recruit=recruit.id, question=question + 1)
    else:
        q = test[question - 1]
        answer = AnswerForm({'question': q, 'recruit': recruit})
        return render(request, 'main/recruit_test.html', {'formset': answer, 'test': q})


def recruit_test_completed(request):
    return render(request, 'main/recruit_test_completed.html')


def recruit_answers(request, recruit):
    recruit = Recruit.objects.get(id=recruit)
    sith = Sith.objects.get(id=request.GET['sith_id'])
    answers = recruit.answer_set.all()
    message = ''

    if request.method == 'POST':
        form = MakeHandForm(request.POST, instance=recruit)
        if form.is_valid():
            if recruit.sith == sith and len(sith.recruit_set.all()) < 3:
                recruit = form.save()
                message = 'Вы сделали рекрута своей рукой тени!' if recruit.sith == sith else 'Вы удалили его из рук тени!'
            else:
                recruit.sith = None
                message = 'У вас уже 3 руки тени! Нельзя иметь больше 3 рук тени'
    if recruit.sith == sith:
        buttonText = 'Убрать из рук тени'
        makeHandForm = MakeHandForm(instance=recruit, initial={'sith': None})
    else:
        buttonText = 'Сделать рукой тени'
        makeHandForm = MakeHandForm(instance=recruit, initial={'sith': sith})
    return render(request, 'main/recruit_answers.html', {'recruit': recruit, 'answers': answers, 'form': makeHandForm,
                                                         'message': message, 'buttonText': buttonText, 'sith_id': sith.id})


def bonus(request):
    with_count = Sith.objects.annotate(Count('recruit'))
    gt1 = Sith.objects.annotate(hand_count=Count('recruit')).filter(hand_count__gt=1)
    return render(request, 'main/bonus.html', {'with_count': with_count, 'gt1': gt1})
