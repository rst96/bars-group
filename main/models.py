from django.db import models
import json


class Planet(models.Model):
    name = models.CharField('Название', max_length=32, primary_key=True)

    class Meta:
        verbose_name = 'Планета'
        verbose_name_plural = 'Планеты'

    def __str__(self):
        return self.name


class Sith(models.Model):
    name = models.CharField('Имя', max_length=32)
    planet = models.ForeignKey(Planet, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Ситх'
        verbose_name_plural = 'Ситхи'

    def __str__(self):
        return self.name


class Recruit(models.Model):
    name = models.CharField('Имя', max_length=32)
    planet = models.ForeignKey(Planet, on_delete=models.CASCADE)
    sith = models.ForeignKey(Sith, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Рекрут'
        verbose_name_plural = 'Рекруты'


class Test(models.Model):
    Planet = models.ForeignKey(Planet, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Тест'
        verbose_name_plural = 'Тесты'

    def __str__(self):
        return self.Planet.name


class Question(models.Model):
    text = models.TextField('Текст')
    test = models.ForeignKey(Test, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

    def __str__(self):
        return self.text


class Answer(models.Model):
    recruit = models.ForeignKey(Recruit, null=False, blank=False, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, null=False, blank=False, on_delete=models.CASCADE)
    value = models.BooleanField('Да', default=False)

    def __str__(self):
        return ' '.join((self.recruit.name, self.question.text, str(self.value)))
