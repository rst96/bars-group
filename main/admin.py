from django.contrib import admin
from .models import Planet, Sith, Test, Recruit, Question, Answer


admin.site.register(Planet)
admin.site.register(Sith)
admin.site.register(Test)
admin.site.register(Recruit)
admin.site.register(Question)
admin.site.register(Answer)
