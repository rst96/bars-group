# Generated by Django 3.0 on 2019-12-09 21:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recruit',
            name='answers',
        ),
        migrations.AlterField(
            model_name='question',
            name='answers',
            field=models.BooleanField(verbose_name='Да'),
        ),
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.BooleanField(verbose_name='Да')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Question')),
                ('recruit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Recruit')),
            ],
        ),
    ]
